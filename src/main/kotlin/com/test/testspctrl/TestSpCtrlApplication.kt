package com.test.testspctrl

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class TestSpCtrlApplication

fun main(args: Array<String>) {
	runApplication<TestSpCtrlApplication>(*args)
}
